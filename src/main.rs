use dotenv::dotenv;
use ethers::{
    contract::abigen,
    core::types::Address,
    providers::{Provider, StreamExt, Ws},
};
use eyre::{Result, WrapErr};
use std::sync::Arc;
use tokio::task;

// 721 - Works mostly with only few decoding errors in between
// abigen!(
//     CONTRACT,
//     r#"[
//     event Approval(address indexed owner, address indexed spender, uint wad)
// ]"#,
// );

// 721- Doesn't work mostly with only few correct payloads in between
// abigen!(
//     CONTRACT,
//     r#"[
//     event Approval(address indexed owner, address indexed spender, uint indexed wad)
// ]"#,
// );

// 721 - Doesn't work mostly with only few correct payloads in between
// abigen!(CONTRACT, "./721.json");

// 720 - Works without errors
abigen!(CONTRACT, "./720.json");

#[tokio::main]
async fn main() -> Result<()> {
    dotenv().ok();

    // 721
    // let weth_address: &str = &std::env::var("ADDRESS_721").unwrap();
    // 720
    let weth_address: &str = &std::env::var("ADDRESS_720").unwrap();

    if let Ok(provider_env) = std::env::var("PROVIDERS") {
        let providers: Vec<&str> = provider_env.split(',').collect();
        let mut provider_iter = providers.into_iter();
        let mut join_handles: Vec<task::JoinHandle<()>> = Vec::new();

        // For each provider, execute...
        while let Some(provider) = provider_iter.next() {
            let provider_name = provider_name(provider.to_string());
            let provider = Provider::<Ws>::connect(provider)
                .await
                .wrap_err("Failed to connect to WebSocket provider")?;
            let client = Arc::new(provider);
            let address: Address = weth_address
                .parse()
                .wrap_err("Failed to parse contract address")?;
            let contract = CONTRACT::new(address, client);

            // Spawn new thread for each provider
            let handle = task::spawn(async move {
                listen_specific_events(provider_name, contract)
                    .await
                    .unwrap(); // Handle errors as appropriate
            });
            join_handles.push(handle);
        }

        for handle in join_handles {
            // Join with main thread and wait...
            handle.await.unwrap();
        }
    } else {
        // TODO: do appropriate error handling
    }
    Ok(())
}

async fn listen_specific_events(
    provider_name: &str,
    contract: CONTRACT<Provider<Ws>>,
) -> Result<()> {
    let events = contract.event::<ApprovalFilter>();
    // Attempt to create the event stream, with detailed error logging
    let mut stream = match events.stream().await {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Failed to create event stream. Error: {:?}", e);
            // You can implement retry logic here if appropriate
            return Err(e.into()); // Convert the error into `eyre::Result` type
        }
    };

    // Process each event
    while let Some(event_result) = stream.next().await {
        match event_result {
            Ok(f) => println!("Event for {}: {f:?}", provider_name),
            Err(e) => eprintln!("Error while receiving event: {:?}", e),
        }
    }

    Ok(())
}

fn provider_name(provider_url: String) -> &'static str {
    if provider_url.contains("alchemy") {
        return "Alchemy";
    } else if provider_url.contains("getblock") {
        return "Getblock";
    } else {
        return "Unknown";
    }
}
